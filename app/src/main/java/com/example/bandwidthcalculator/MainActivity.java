package one.eight192.bandwidthcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    float bandwidth;
    double packetloss;
    double box; //Placeholder before loss calculation
    double finale; //finale result print value
    double converter; //var used for conversion of data
    double output;


    RadioButton seconds, minutes, hours, days, Kbps, KBps, Mbps, MBps, Gbps, GBps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Time options
        seconds = (RadioButton) findViewById(R.id.seconds);
        minutes = (RadioButton) findViewById(R.id.minutes);
        hours = (RadioButton) findViewById(R.id.hours);
        days = (RadioButton) findViewById(R.id.days);


        //Bandwidth input option
        Kbps = (RadioButton) findViewById(R.id.Kbps);
        KBps = (RadioButton) findViewById(R.id.KBps);
        Mbps = (RadioButton) findViewById(R.id.Mbps);
        MBps = (RadioButton) findViewById(R.id.MBps);
        Gbps = (RadioButton) findViewById(R.id.Gbps);
        GBps = (RadioButton) findViewById(R.id.GBps);


        Spinner spinner = (Spinner) findViewById(R.id.output_unit_spinner);
        spinner.setOnItemSelectedListener(this);

        List<String> outputs = new ArrayList<String>();
        outputs.add("Bytes");
        outputs.add("Kilobytes");
        outputs.add("Megabytes");
        outputs.add("Gigabytes");
        outputs.add("Terabytes");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, outputs);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);



        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText Bandwidth_KB_editText = (EditText) findViewById(R.id.Bandwidth_editText);
                EditText loss_editText = (EditText) findViewById(R.id.loss_editText);
                EditText time_in_s_editText2 = (EditText) findViewById(R.id.time_editText2);
                TextView result_textView = (TextView) findViewById(R.id.result_textView);


                if (Kbps.isChecked()){
                    converter = 0.000000125;
                }
                    else if (KBps.isChecked()){
                    converter = 0.000001;
                }
                    else if (Mbps.isChecked()){
                    converter = 0.000125;
                }
                    else if (MBps.isChecked()){
                    converter = 0.001;
                }
                    else if (Gbps.isChecked()){
                    converter = 0.125;
                }
                    else if (GBps.isChecked()){
                    converter = 1;
                }


                if (seconds.isChecked()) {
                    int time = 1;
                    Float.parseFloat(time_in_s_editText2.getText().toString());
                    bandwidth = Float.parseFloat(Bandwidth_KB_editText.getText().toString());
                    packetloss = Double.parseDouble(loss_editText.getText().toString());
                    box = bandwidth * time * converter;

                    if (packetloss >=100) {
                        finale = 0;
                        result_textView.setText(finale + "");

                    }else if(packetloss==0){
                        finale = box*output;
                        result_textView.setText(finale +"");
                        }

                    else {
                        double result = box * packetloss / 100;
                        finale = box - result*output;
                        result_textView.setText(finale + "");
                    }
                }

                else if (minutes.isChecked()) {
                    int time = 60;
                    Float.parseFloat(time_in_s_editText2.getText().toString());
                    bandwidth = Float.parseFloat(Bandwidth_KB_editText.getText().toString());
                    packetloss = Double.parseDouble(loss_editText.getText().toString());
                    box = bandwidth * time * converter;
                    if (packetloss >= 100) {
                        finale = 0;
                        result_textView.setText(finale + "");

                    } else if (packetloss == 0) {
                        finale = box * output;
                        result_textView.setText(finale + "");
                    } else {
                        double result = box * packetloss / 100;
                        finale = box - result * output;
                        result_textView.setText(finale + "");
                    }

                }

                else if (hours.isChecked()) {
                    int time = 60 * 60;
                    Float.parseFloat(time_in_s_editText2.getText().toString());
                    bandwidth = Float.parseFloat(Bandwidth_KB_editText.getText().toString());
                    packetloss = Double.parseDouble(loss_editText.getText().toString());
                    box = bandwidth * time * converter;
                        if (packetloss >=100) {
                            finale = 0;
                            result_textView.setText(finale + "");

                        }else if(packetloss==0){
                            finale = box*output;
                            result_textView.setText(finale +"");
                        }

                        else {
                            double result = box * packetloss / 100;
                            finale = box - result*output;
                            result_textView.setText(finale + "");
                        }
                }
                else if (days.isChecked()) {
                    int time = 60 * 60 * 24;
                    Float.parseFloat(time_in_s_editText2.getText().toString());
                    bandwidth = Float.parseFloat(Bandwidth_KB_editText.getText().toString());
                    packetloss = Double.parseDouble(loss_editText.getText().toString());
                    box = bandwidth * time * converter;
                        if (packetloss >=100) {
                            finale = 0;
                            result_textView.setText(finale + "");

                        }else if(packetloss==0){
                            finale = box*output;
                            result_textView.setText(finale +"");
                        }

                        else {
                            double result = box * packetloss / 100;
                            finale = box - result*output;
                            result_textView.setText(finale + "");
                        }
                }

            }




        });


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String item = parent.getItemAtPosition(position).toString();

        if (item == "Bytes"){
            //GB to byte
            output = 1000000000;
        }
        else if (item == "Kilobytes"){

            output = 1000000;
        }
        else if (item == "Megabytes"){
             output = 1000;
        }
        else if (item == "Gigabytes"){
            output = 1;
        }
        else if (item == "Terabytes"){
            output = 0.001;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {



    }
}
